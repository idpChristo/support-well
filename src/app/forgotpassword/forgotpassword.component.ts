import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { UserService } from "../services/user.service";
import { UserProfile } from "../models/UserProfileModel";
import { AsyncLocalStorage } from 'angular2-async-local-storage'
import { Observable } from "rxjs-compat";

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  version: any;
  appversion: any;
  dataReturned: any;
  user: any;
  result: any;
  username: string;
  constructor(public router: Router, public http: HttpClient, public service: UserService) {
    this.user = new UserProfile(0, '', '', '', '', '','');
    localStorage.clear();
    localStorage.removeItem("currentUser");
  }
  login() {
    this.router.navigateByUrl('/login')
  }

  reset(user) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/RequestPassword?UserCell=' + user.UserCellPhone).subscribe(res => {
      alert('Please look out for a sms with your password Request.');
      this.router.navigateByUrl('/login');
    })
  }

  ngOnInit() {
  }

}
