import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { TransportData } from "../models/transportmodel";

@Component({
  selector: 'app-transport-modal',
  templateUrl: './transport-modal.component.html',
  styleUrls: ['./transport-modal.component.scss']
})
export class TransportModalComponent implements OnInit {

  BookingID: any;
  ServeType: any;
  Slots: any;
  Slot: any;
  StartTime: any;
  EndTime: any;
  categoryList: any;
  slots: any;
  Type: any;
  dataReturned: any;
  eventList: any;
  list: any;
  list1: any;
  list2: any;
  username: any;
  eventName: string;
  eventlist: string;
  typelist: any;
  fileUrl: any;
  FullName: any;
  EventsName: any;
  EventsType: any;
  show: boolean;
  leaving: string;
  arriving: string;

  constructor(public dialogRef: MatDialogRef<TransportModalComponent>, @Inject(MAT_DIALOG_DATA) public data: TransportData, public http: HttpClient) { 
    
    this.getslots();
  }

  SlotChanged(selectedValue: string) {
    this.Slot = selectedValue;
  }
  
  add(Leaving,Arriving) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddTransportSlot?Slots=' + this.Slot + '&Leaving=' + Leaving + '&Arriving=' + Arriving).subscribe(res => {

    })
    this.onNoClick();
  }

  getslots() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/getSlots').subscribe(res => {
      this.slots = res;
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
