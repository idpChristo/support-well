import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: 'app-coachhome',
  templateUrl: './coachhome.component.html',
  styleUrls: ['./coachhome.component.scss']
})
export class CoachhomeComponent implements OnInit {

  constructor(
    public http: HttpClient,
    public router: Router
  ) { }

  registers() {
    this.router.navigateByUrl('/registers');
  }

  terms() {
    this.router.navigateByUrl('/terms');
  }

  teams() {
    this.router.navigateByUrl('/teams');
  }

  practice() {
    this.router.navigateByUrl('/practice');
  }

  ngOnInit() {
  }

}
