import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { DxDateBoxModule } from 'devextreme-angular';

@Component({
  selector: 'app-termsmodal',
  templateUrl: './termsmodal.component.html',
  styleUrls: ['./termsmodal.component.scss']
})
export class TermsmodalComponent implements OnInit {
  terms: any;
  value: Date = new Date(2019, 3, 27);
  now: Date = new Date();
  firstWorkDay2017: Date = new Date(2017, 0, 3);
  min: Date = new Date(2019, 0, 1);
  dateClear = new Date(2019, 11, 1, 6);
  disabledDates: Date[];


  constructor(public http: HttpClient) { 
    this.getTerms();
  }

  ngOnInit() {
   
  }
  getTerms() {
    this.http.get('https://localhost:44308/api/getTerms').subscribe(res => {
      this.terms = res;
    })
  }
}
