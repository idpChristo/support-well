import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistersmodalComponent } from './registersmodal.component';

describe('RegistersmodalComponent', () => {
  let component: RegistersmodalComponent;
  let fixture: ComponentFixture<RegistersmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistersmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistersmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
