import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsmodalComponent } from './teamsmodal.component';

describe('TeamsmodalComponent', () => {
  let component: TeamsmodalComponent;
  let fixture: ComponentFixture<TeamsmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
