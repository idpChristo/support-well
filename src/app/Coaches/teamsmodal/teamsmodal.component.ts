import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BsModalService } from 'ngx-bootstrap';
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-teamsmodal',
  templateUrl: './teamsmodal.component.html',
  styleUrls: ['./teamsmodal.component.scss']
})
export class TeamsmodalComponent implements OnInit {

  public onClose: Subject<boolean>;
  player: any;
  team: any;
  ddlPlayer: any;
  ddlTeam: any;
  userID: any;
  bsModalRef: any;

  constructor(
    public http: HttpClient,
    private _bsModalRef: BsModalRef
  ) {
    this.userID = this.getSelectedUserName();
    this.getPlayer()
    this.getTeam();
  }

  PlayerfilterChanged(selectedValue: string) {
    this.ddlPlayer = selectedValue;
    console.log('Player ID: ' + this.ddlPlayer)
  }
  TeamfilterChanged(selectedValue: string) {
    this.ddlTeam = selectedValue;
    console.log('Team ID: ' + this.ddlTeam)
  }

  public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }

  addPlayer() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddPlayer?CoachID=' + this.userID + '&StudentID=' + this.ddlPlayer + '&TeamID=' + this.ddlTeam).subscribe(res => {

    })
    this.onClose.next(true);
    this._bsModalRef.hide();
  }

  getPlayer() {
    this.player = [];
    this.http.get('https://localhost:44308/api/getPlayer').subscribe(res => {
      this.player = res;
    })
  }
  public onConfirm(): void {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddPlayer?CoachID=' + this.userID + '&StudentID=' + this.ddlPlayer + '&TeamID=' + this.ddlTeam).subscribe(res => {
      this.onClose.next(true);
      this._bsModalRef.hide();
    })
  }
  getTeam() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetTeams').subscribe(res => {
      this.team = res;
    })
  }

  public ngOnInit(): void {
    this.onClose = new Subject();
  }

}
