import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BsModalService } from 'ngx-bootstrap';
import { TeamsmodalComponent } from '../teamsmodal/teamsmodal.component';

@Component({
  selector: 'app-registers',
  templateUrl: './registers.component.html',
  styleUrls: ['./registers.component.scss']
})
export class RegistersComponent implements OnInit {
  bsModalRef: any;

  constructor(private modalService: BsModalService) { 

  }

  OpenPracticeModal() {
    // Open the PracticeModal 
      const initialState = {
        id: '' // ID to be passed if you want to pass data and edit team meamber
      };
      this.bsModalRef = this.modalService.show(TeamsmodalComponent,  {class: 'modal-lg'});
      this.bsModalRef.content.closeBtnName = 'Close';
    
  }

  OpenMatchModal() { 
    // Open MatchModal
      const initialState = {
        id: '' // ID to be passed if you want to pass data and edit team meamber
      };
      this.bsModalRef = this.modalService.show(TeamsmodalComponent, {class: 'modal-lg'});
      this.bsModalRef.content.closeBtnName = 'Close';
    
  }

  ngOnInit() {
  }

}
