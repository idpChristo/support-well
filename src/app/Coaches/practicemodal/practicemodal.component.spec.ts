import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticemodalComponent } from './practicemodal.component';

describe('PracticemodalComponent', () => {
  let component: PracticemodalComponent;
  let fixture: ComponentFixture<PracticemodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticemodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticemodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
