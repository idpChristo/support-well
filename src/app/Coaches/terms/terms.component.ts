import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { TermsmodalComponent } from "../termsmodal/termsmodal.component";

import { HttpClient } from "@angular/common/http";

let appointments: Appointment[] = []

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  year: number = new Date().getFullYear();
  month: number = new Date().getMonth();
  day: number = new Date().getDay();
  terms: any;
  list: any = {};
  userId = '';
  appointmentsData: Appointment[];
  currentDate: Date = new Date(this.year, this.month, this.day);
  bsModalRef: any;

  constructor(private modalService: BsModalService, public http: HttpClient) {
    this.appointmentsData = this.getAppointments();
  }

  getAppointments(): Appointment[] {
    return appointments;
  }

  getTerms() {
    this.http.get('https://localhost:44308/api/getTerms').subscribe(res => {
      this.terms = res;
    })
  }

  openModal() {
    const initialState = {
      id: '' // ID to be passed if you want to pass data and edit team meamber
    };
    this.bsModalRef = this.modalService.show(TermsmodalComponent, { class: 'modal-lg' });
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  ngOnInit() {
  }

}

export class Appointment {
  text: string;
  startDate: Date;
  endDate: Date;
  allDay?: boolean;
}