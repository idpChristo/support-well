import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { TeamsmodalComponent } from '../teamsmodal/teamsmodal.component';
import { ModalDialogService } from 'ngx-modal-dialog';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PracticeComponent } from '../practice/practice.component';

import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  public modalRef: BsModalRef;
  list: any = []
  userID: any;
  bsModalRef: any;
  benched: boolean = false;
  starting: boolean = false;

  constructor(
    public http: HttpClient,
    public router: Router,
    private modalService: BsModalService
  ) {
    this.userID = this.getSelectedUserName();
    this.getMyTeam();
  }

  openConfirmDialog() {
    this.modalRef = this.modalService.show(TeamsmodalComponent);
    this.modalRef.content.onClose.subscribe(result => {
      this.getMyTeam();
    })
}
  //TO DO: When the coach logged in the System must return all the students that is part of the coaches teams.
  getMyTeam() {
    this.http.get('https://localhost:44308/api/getMyTeams?UserID=' + this.userID).subscribe(res => {
      this.list = res;
    })
  }

  benchTeamMember(TeamID) {
    this.http.get('https://localhost:44308/api/BenchPlayer?StudentID=' + TeamID).subscribe(res => {
      this.getMyTeam();
    })
  }

  startingLine(TeamID) {
    this.http.get('https://localhost:44308/api/StartPlayer?StudentID=' + TeamID).subscribe(res => {
      this.getMyTeam();
    })
  }
  removePlayer(TeamID) {
    this.http.get('https://localhost:44308/api/RemovePlayer?StudentID=' + TeamID).subscribe(res => {
      this.getMyTeam();
    })
  }

  public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }

  openModal() {
    const initialState = {
      id: '' // ID to be passed if you want to pass data and edit team meamber
    };
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef = this.modalService.show(TeamsmodalComponent, { initialState });
    // this.bsModalRef.content.onClose.subscribe(result => {
    //   this.getMyTeam();
    // })

  }


  ngOnInit() {
  }

}
