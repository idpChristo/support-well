import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap';
import { PracticemodalComponent } from "../practicemodal/practicemodal.component";

let appointments: Appointment[] = []

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.scss']
})
export class PracticeComponent implements OnInit {

  year: number = new Date().getFullYear();
  month: number = new Date().getMonth();
  day: number = new Date().getDay(); 

  list: any = {};
  userId ='';
  appointmentsData: Appointment[];
  currentDate: Date = new Date(this.year, this.month, this.day);
  bsModalRef: any;

  constructor(
    public http: HttpClient,
    public router: Router,
    private modalService: BsModalService
  ) { 
    this.appointmentsData = this.getAppointments();
  }


  getAppointments(): Appointment[] {
    return appointments;
}

  getMyTeamPractices(UserID) {
    this.http.get('').subscribe(res => {
      this.list = res;
    })
  }

  openModal() {
    const initialState = {
      id: '' // ID to be passed if you want to pass data and edit team meamber
    };
    this.bsModalRef = this.modalService.show(PracticemodalComponent, {class: 'modal-lg'});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  ngOnInit() {
  }

}
export class Appointment {
  text: string;
  startDate: Date;
  endDate: Date;
  allDay?: boolean;
}