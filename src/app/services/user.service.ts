import { Injectable } from '@angular/core';
import { UserProfile } from '../models/UserProfileModel'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  result: any;
  constructor(public http: HttpClient) { }
  // User Registers on the system we pass the user model through
  register(UserFirstName,UserLastName,UserEmailAddress,UserCellPhone,UserPassword) {
    const userprofile: UserProfile = {
      UserProfileID: 0,
      UserFirstName: UserFirstName,
      UserLastName: UserLastName,
      UserEmailAddress: UserEmailAddress,
      UserCellPhone: UserCellPhone,
      UserPassword: UserPassword,
      ConfirmPassword: ''
    };

    const body = userprofile;
    console.log('Body Context: ' + JSON.stringify(body));
  //  return this.http.post('https://supportwellapi.idp-apps.com:449/api/register', body);
   return this.http.post('https://supportwellapi.idp-apps.com:449/api/register', body);
  }

  // User Logs on to the system we pass the user model through
  login(user) {

    const userprofile: UserProfile = {
      UserProfileID: 0,
      UserFirstName: user.UserFirstName,
      UserLastName: user.UserLastName,
      UserEmailAddress: user.UserEmailAddress,
      UserCellPhone: user.UserCellPhone,
      UserPassword: user.UserPassword,
      ConfirmPassword: ''
    };

    const body = userprofile;
    console.log('Body Context: ' + JSON.stringify(body));
    return this.http.get('https://supportwellapi.idp-apps.com:449/api/login?username=' + user.UserCellPhone + '&password=' + user.UserPassword)
      .toPromise()
      .then(res => {
        console.log('First Result: ' + res)
      })
      .catch();
  }
}
