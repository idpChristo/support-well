import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Component({
  selector: 'app-pendingusers',
  templateUrl: './pendingusers.component.html',
  styleUrls: ['./pendingusers.component.scss']
})
export class PendingusersComponent implements OnInit {
  users: any;
  p : number = 1;
  
  constructor(public http: HttpClient) { 
    this.getPendingUsers();
  }

 getPendingUsers() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetPendingUsers').subscribe(res => {
      this.users = res;
    })
  }

  updatePendingUser(item) {
    this.users = null;
    this.users = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/UpdatePendingUser?UserId=' + item.UserID).subscribe(res => {
        this.users = res;
    })
  }
  ngOnInit() {
  }

}
