import { Component, OnInit, ViewContainerRef,TemplateRef,ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
//import { ModalDialogService } from 'ngx-modal-dialog';
import { FixturesmodalComponent } from '../fixturesmodal/fixturesmodal.component';


import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.scss']
})
export class FixturesComponent implements OnInit {
  events: any;
  info : any;
  dataReturned:any;
  username: any;
  fileUrl: any;
  modalRef: BsModalRef;
  bsModalRef:any;
  p : number = 1; 

  

  @ViewChild(TemplateRef) tpl: TemplateRef<any>;

  constructor(public router: Router, public http: HttpClient,public viewRef: ViewContainerRef,private modalService: BsModalService) {
    this.getEvents();
    this.username = this.getSelectedUserName();
   }
   

   getEvents() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetEvents').subscribe(res => {
      this.info = res;
    });
  }
  public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }

  openModalWithComponent(EventId) {
    const initialState = {
      list: [
        'Open Fixtures Modal',
      ],
      id: EventId
    };
    this.bsModalRef = this.modalService.show(FixturesmodalComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }
  ngOnInit() {

  }

}
