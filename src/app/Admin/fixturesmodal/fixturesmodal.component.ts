import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-fixturesmodal',
  templateUrl: './fixturesmodal.component.html',
  styleUrls: ['./fixturesmodal.component.scss']
})
export class FixturesmodalComponent implements OnInit {

  id: string = "";
  list: any;
  constructor(public bsModalRef: BsModalRef, public http:HttpClient) { }

  ngOnInit() {
    this.getFixtures();
  }

  getFixtures() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetEventsByID?EventID=' + this.id).subscribe(res => {
      if (Object.keys(res).length === 0) {
        this.list = null;
      }
      else {
        this.list = res;
      }
    })
  }

}
