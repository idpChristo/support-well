import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { Router } from '@angular/router'
import { HttpHeaders, HttpRequest } from '@angular/common/http';
import { Volunteer } from 'src/app/models/ServingModel';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { MyModalComponent } from '../../my-modal/my-modal.component';
import { AddModalComponent } from '../../add-modal/add-modal.component';

@Component({
  selector: 'app-volunteer',
  templateUrl: './volunteer.component.html',
  styleUrls: ['./volunteer.component.scss']
})
export class VolunteerComponent implements OnInit {
  name: string;
 // color: string;
  list: any;
  p: number = 1;
  dataReturned: any;
  eventList: any;
  categoryList: any;
  list1: any;
  list2: any;
  username: any;
  eventName: string;
  eventlist: string;
  typelist: any;
  GroupedItems: Volunteer[];
  fileUrl: any;
  FullName: any;
  EventsName: any;
  EventsType: any;
  show: boolean;
  closeResult: string;
  modalOptions: NgbModalOptions;

  constructor(public http: HttpClient, public router: Router, public dialog: MatDialog) {
    this.getCategories();
    this.getAllSlots();
    this.getAllSlots1();
    this.getAllSlots2();
    this.eventlist = null;
    this.username = this.getSelectedUserName();
    this.getUserProfileImage();
    this.GetUserName();
    this.EventsType = 'All';

  }
  GetUserName() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetUserName?UserName=' + this.username).subscribe(res => {
      this.FullName = res;

    })
  }

  public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }
  getEventName() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetEvents').subscribe(res => {

      this.eventList = res;

    })
  }
  getUserProfileImage() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetUserProfileImage?UserName=' + this.username).subscribe(res => {
      if (res === null) {
        this.fileUrl = res;
      }
      else {
        this.fileUrl = 'https://supportwellapi.idp-apps.com:449/Images/' + res;
      }

    });
  }
  EventChanged(selectedValue: string) {
    const Events = selectedValue;
    const eventList = Events
    this.GroupedItems = null;
    this.GroupedItems = [];
    if (Events == null) {

    }
    else {
      this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlotsEventNameAndType?EventName=' + Events + '&Type=' + this.typelist).subscribe(res => {
        this.list = res;

        this.GroupedItems = this.list.reduce((r, { BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots }) => {
          if (!r.some(o => o.BookingID == BookingID)) {

            r.push({ BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, GroupedOrder: this.list.filter(v => v.BookingID == BookingID) });
          }
          return r;
        }, []);

      })

    }
  }

  trackByFn(index, item) {
    if (index <= item.BookedSpots) {
      this.show = false;
    }
    if (index >= item.BookedSpots) {
      this.show = true;
    }
    return this.show;
  }

  TypeChanged(selectedValue: string) {
    const Events = selectedValue;
    const eventList = Events
    this.GroupedItems = null;
    this.GroupedItems = [];
    if (Events == 'All') {
      this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlots').subscribe(res => {
        this.list = res;
        let i: number = 0;


        this.GroupedItems = this.list.reduce((r, { BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, BookingUserID }) => {
          if (!r.some(o => o.BookingID == BookingID)) {
            this.EventsName = EventName;
            r.push({ BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, BookingUserID, GroupedOrder: this.list.filter(v => v.BookingID == BookingID) });
          }
          return r;
        }, []);
      })
    }
    else {
      console.log('Event Before API Call: ' + Events)
      this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlotsType?Type=' + Events).subscribe(res => {
        this.list = res;
        let i: number = 0;


        this.GroupedItems = this.list.reduce((r, { BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots }) => {
          if (!r.some(o => o.BookingID == BookingID)) {
            this.EventsName = EventName;
            r.push({ BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, GroupedOrder: this.list.filter(v => v.BookingID == BookingID) });
          }
          return r;
        }, []);
      })

    }
  }

  UnBook(item) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/RemoveBooking?EventName=' + item.BookingID + '&Username=' + this.username).subscribe(res => {
      this.list = res;
    })
    setTimeout(() => {
      this.getAllSlots();
    }, 2000);
  }
  UnBook1(item) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/RemoveBooking1?EventName=' + item.BookingName + '&Username=' + this.username).subscribe(res => {
      this.list1 = res;
    })

    this.getAllSlots();
    this.getAllSlots1();
    this.getAllSlots2();
  }
  UnBook2(item) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/RemoveBooking2?EventName=' + item.BookingName + '&Username=' + this.username).subscribe(res => {
      this.list2 = res;
    })

    this.getAllSlots();
    this.getAllSlots1();
    this.getAllSlots2();
  }

  Book(item) {

    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddBooking?EventName=' + item.BookingID + '&Username=' + this.username).subscribe(res => {
      this.list = res;
      if (res === true) {
        alert('you have already volunteered for this event.')
      }
    })
    setTimeout(() => {
      this.getAllSlots();
    }, 2000);
  }

  Book1(item) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddBooking1?EventName=' + item.BookingName + '&Username=' + this.username).subscribe(res => {
      this.list1 = res;
    })
    this.getAllSlots();
    this.getAllSlots1();
    this.getAllSlots2();
  }
  Book2(item) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddBooking2?EventName=' + item.BookingName + '&Username=' + this.username).subscribe(res => {
      this.list2 = res;
    })
    this.getAllSlots();
    this.getAllSlots1();
    this.getAllSlots2();
  }

  getCategories() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetCategories').subscribe(res => {
      this.categoryList = res;
    })
  }

  getAllSlots() {
    this.list = null;
    this.list = [];
    this.GroupedItems = null;
    this.GroupedItems = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlots').subscribe(res => {
      this.list = res;
      let i: number = 0;

      this.GroupedItems = this.list.reduce((r, { BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, BookingsTime }) => {
        if (!r.some(o => o.BookingID == BookingID)) {
          this.EventsName = EventName;
          r.push({ BookingName, BookingID, EventName, EventTimeSlot, BookingSpots, BookedSpots, BookingsTime, GroupedOrder: this.list.filter(v => v.BookingID == BookingID) });
        }
        return r;
      }, []);
      //    debugger;
    })
  }
  getAllSlots1() {
    this.list1 = null;
    this.list1 = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlots1').subscribe(res => {
      this.list1 = res;
    })
  }
  getAllSlots2() {
    this.list2 = null;
    this.list2 = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlots2').subscribe(res => {
      this.list2 = res;
    })
  }
  getMySlots() {
    this.list = null;
    this.list = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/getMySlots?UserName=' + this.username).subscribe(res => {
      this.list = res;
      // this.list = null;
      // this.list = [];
      // this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllSlots').subscribe(res => {
      //   this.list = res;
    })
  }

  ngOnInit() {
  }
  openDialog(BookingID,BookingName,BookingSpots): void {
    const dialogRef = this.dialog.open(MyModalComponent, {
      width: '450px',
      data: { BookingID: BookingID, BookingName: BookingName ,Slots:BookingSpots}
    });

    dialogRef.afterClosed().subscribe(res => {
      //this.color = res;
      this.getAllSlots();
    });
  }
  open(): void {
    const dialogRef = this.dialog.open(AddModalComponent, {
      width: '450px',
      data: { BookingID: '', BookingName: '',Slots:''}
    });

    dialogRef.afterClosed().subscribe(res => {
      //this.color = res;
      this.getAllSlots();
    });
  }

}
