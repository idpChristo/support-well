import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  list: any;
  p: number = 1;
  events: any;
  info : any;
  dataReturned:any;
  username: any;
  fileUrl: any;

  constructor(public router: Router, public http: HttpClient) { 
    this.getEvents();
    this.username = this.getSelectedUserName()
  }
  
  getEvents() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetEvents').subscribe(res => {
      
      this.info = res;
    });
  }
  public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }

  ngOnInit() {
  }

}
