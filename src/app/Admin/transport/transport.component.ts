import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { Router } from '@angular/router'
import { HttpHeaders, HttpRequest } from '@angular/common/http';
import { Volunteer } from 'src/app/models/ServingModel';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { TransportModalComponent} from '../../transport-modal/transport-modal.component';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['./transport.component.scss']
})
export class TransportComponent implements OnInit {

  fileUrl: any;
  username: any;
  list:any;
  GroupedItems: Transport[]
  transportType:any;
  show: boolean;
  dataReturned:any;

  constructor(public router: Router, public http: HttpClient, public dialog: MatDialog) {
    this.username = this.getSelectedUserName();
    this.getUserProfileImage();
    this.getAllSlots();
   }

   public getSelectedUserName(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser.token;
    return token;
  }
  getUserProfileImage() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetUserProfileImage?UserName=' + this.username).subscribe(res => {
      if(res === null)
      {
        this.fileUrl = res;  
      }
      else{
        this.fileUrl = 'https://supportwellapi.idp-apps.com:449/Images/' + res;
      }
      
    });
  }
  getAllSlots() {
    this.list = null;
    this.list = [];
    this.GroupedItems = null;
    this.GroupedItems = [];
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetAllTransport').subscribe(res => {
      this.list = res;
      let i: number = 0;

      this.GroupedItems = this.list.reduce((r, { TransportID,Transport, TrasportBookingUserID, Leaving, Arriving, Slots, BookedSpots }) => {
        if (!r.some(o => o.TransportID == TransportID)) {
          this.transportType = Transport;
          r.push({TransportID, Transport, TrasportBookingUserID, Leaving, Arriving, Slots, BookedSpots, GroupedOrder: this.list.filter(v => v.TransportID == TransportID) });
        }
        return r;
      }, []);
      //    debugger;
    })
  }

  trackByFn(index, item) {
    if (index <= item.BookedSpots) {
    this.show = false;
    }
    if(index >= item.BookedSpots)
    {
      this.show = true;
    }
    return this.show;
  }
  open(): void {
    const dialogRef = this.dialog.open(TransportModalComponent, {
      width: '450px',
      data: { BookingID: '', BookingName: '',Slots:''}
    });

    dialogRef.afterClosed().subscribe(res => {
      //this.color = res;
      this.getAllSlots();
    });
  }

  openDialog(BookID,BookName,BookSpot){
    console.log(BookID, BookName, BookSpot)
  }

  ngOnInit() {
  }
 
}
