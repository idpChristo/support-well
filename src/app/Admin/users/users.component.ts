import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  list: any;
  p: number = 1;
  
  constructor(public http: HttpClient) { 
     this.getUsers();
  }
  reset(user) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/RequestPassword?UserCell=' + user).subscribe(res => {
      alert('SMS send to the user with their details.');
    })
  }

  getUsers() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetUsers').subscribe(res => {
    this.list = res;
    });
  }
  ngOnInit() {
  }

}
