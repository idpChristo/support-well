import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './admin/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { HttpClientModule } from "@angular/common/http";
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule, DateAdapter } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { UsersComponent } from './admin/users/users.component';
import { PendingusersComponent } from './admin/pendingusers/pendingusers.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { TransportComponent } from './admin/transport/transport.component';
import { VolunteerComponent } from './admin/volunteer/volunteer.component';
import { ScheduleComponent } from './admin/schedule/schedule.component';
import { FixturesComponent } from './admin/fixtures/fixtures.component';
import { AdminComponent } from './admin/admin/admin.component';
import { ProfileComponent } from './admin/profile/profile.component'; // <-- import the module

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MyModalComponent } from './my-modal/my-modal.component';
import { AddModalComponent } from './add-modal/add-modal.component';
import { TransportModalComponent } from './transport-modal/transport-modal.component';
import { TeamsComponent } from './coaches/teams/teams.component';
import { ModalDialogModule } from 'ngx-modal-dialog';
import { TransportmodalComponent } from './transportmodal/transportmodal.component';
import { FixturesmodalComponent } from './admin/fixturesmodal/fixturesmodal.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ModalModule } from "ngx-bootstrap";
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { CoachhomeComponent } from './coaches/coachhome/coachhome.component';
import { RegistersComponent } from './coaches/registers/registers.component';
import { TermsComponent } from './coaches/terms/terms.component';
import { PracticeComponent } from './coaches/practice/practice.component';
import { TeamsmodalComponent } from './coaches/teamsmodal/teamsmodal.component';
import { PracticemodalComponent } from './coaches/practicemodal/practicemodal.component';
import { TermsmodalComponent } from './coaches/termsmodal/termsmodal.component';
import { RegistersmodalComponent } from './coaches/registersmodal/registersmodal.component';
import { DxSchedulerModule } from 'devextreme-angular';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DxDateBoxModule } from 'devextreme-angular';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UsersComponent,
    TransportComponent,
    VolunteerComponent,
    ScheduleComponent,
    FixturesComponent,
    AdminComponent,
    ProfileComponent,
    MyModalComponent,
    AddModalComponent,
    TransportModalComponent,
    TeamsComponent,
    FixturesmodalComponent,
    ForgotpasswordComponent,
    PendingusersComponent,
    CoachhomeComponent,
    RegistersComponent,
    TermsComponent,
    PracticeComponent,
    TeamsmodalComponent,
    PracticemodalComponent,
    TermsmodalComponent,
    RegistersmodalComponent
  ],
  imports: [
    MatButtonModule, MatIconModule, MatSidenavModule, MatListModule, MatToolbarModule, MatDatepickerModule,
    MatNativeDateModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbModalModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    DxSchedulerModule,
    DxDateBoxModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(), 
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [MyModalComponent, AddModalComponent, TransportModalComponent, FixturesmodalComponent, TeamsmodalComponent, RegistersmodalComponent, TermsmodalComponent, PracticemodalComponent]
})
export class AppModule { }
