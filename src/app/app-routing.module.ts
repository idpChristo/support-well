import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './admin/home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './admin/users/users.component';
import { PendingusersComponent } from './admin/pendingusers/pendingusers.component';
import { ScheduleComponent } from './admin/schedule/schedule.component';
import { FixturesComponent } from './admin/fixtures/fixtures.component';
import { TransportComponent } from './admin/transport/transport.component';
import { ProfileComponent } from './admin/profile/profile.component';
import { VolunteerComponent } from './admin/volunteer/volunteer.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { CoachhomeComponent } from './coaches/coachhome/coachhome.component';
import { PracticeComponent } from './coaches/practice/practice.component';
import { RegistersComponent } from './coaches/registers/registers.component';
import { TeamsComponent } from './coaches/teams/teams.component';
import { TermsComponent } from './coaches/terms/terms.component';
import { Login2Component } from './login2/login2.component';

const routes: Routes = [
  { path: 'coachhome', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'pending', component: PendingusersComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: 'fixtures', component: FixturesComponent },
  { path: 'transport', component: TransportComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'volunteer', component: VolunteerComponent},
  { path: 'login', component: LoginComponent },
  { path: 'login2', component: Login2Component },
  { path: 'register', component: RegisterComponent },
  { path: 'coach', component: CoachhomeComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'practice', component: PracticeComponent },
  { path: 'registers', component: RegistersComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'terms', component: TermsComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  // { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
