export class Volunteer {
    BookingID: number;
    BookingName: string;
    EventName: string;
    BookingSpots: number;
    EventTimeSlot: string;
    UserFirstName: string;
    UserLastName: string;
    GroupedVolunteer?: Volunteer[];
    BookedSlots: number;
    BookedSpots: number;
}
