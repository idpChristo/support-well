export class UserProfile{
    constructor(
          public  UserProfileID: number,
          public  UserFirstName : string,
          public  UserLastName : string,
          public  UserEmailAddress : string,
          public  UserCellPhone : string,
          public  UserPassword : string,
          public ConfirmPassword:string
    ){}
  }
  
  export class UserProfileDetails{
    constructor(
          public  UserProfileID: string,
          public  UserFirstName : string,
          public  UserLastName : string,
          public  UserEmailAddress : string,
          public  UserCellPhone : string,
          
    ){}
  }
  export class UserVehicleModel{
    constructor(
          public  VehicleName : string,
          public  VehicleRegistration : string,
          public  VehicleColour : string,
          public  Seats : string,
          public  UserCellPhone : string,
    ){}
  }