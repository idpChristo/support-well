import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { FormGroup, FormBuilder, FormControl, Validators, Form, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AsyncLocalStorage } from 'angular2-async-local-storage'
import { Observable } from "rxjs-compat";


@Component({
  selector: 'app-login2',
  templateUrl: './login2.component.html',
  styleUrls: ['./login2.component.scss']
})
export class Login2Component implements OnInit {

  version: any;
  appversion: any;
  dataReturned: any;
  user: any;
  result: any;
  usernames: string;
  password: string;
  loginForm: FormGroup;

  error_messages = {
    'usernames': [
      { type: 'required', message:'Your Cellphone number is required' },
      { type: 'minlength', message:'Cellphone minimum length is 10 Numbers' },
      { type: 'maxlength', message:'Cellphone maximum length is 13 Numbers' },
    ],
    'password': [
      { type: 'required', message:'Your Password is required' },
      { type: 'minlength', message:'Password minimun length is 4 Characters' },
      { type: 'maxlength', message:'Password maximum length is 99 Characters' },
    ]
  }

  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public http: HttpClient,
  ) { 

    localStorage.clear();
    localStorage.removeItem("currentUser");


    this.loginForm = this.formBuilder.group({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(99)
      ])),
      usernames: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(13),
      
      ])),
      test: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(13),
      
      ]))

    });

  }

  login(usernames,password) {
    //const password = this.loginForm.get('password').value;
    //const usernames = this.loginForm.get('usernames').value;
    const test = this.loginForm.get('test').value;
    
    return this.http.get('https://supportwellapi.idp-apps.com:449/api/login?username=' + usernames + '&password=' + password)
      .toPromise()
      .then(res => {
        this.result = res;
        console.log('Status: ' + res)
        if (res == '1') {
          localStorage.setItem('currentUser', JSON.stringify({ token: usernames, name: usernames }));
          this.router.navigate(['/home']);
          //this.router.navigate(['/coach']);
        }
        if (res == '4') {
          localStorage.setItem('currentUser', JSON.stringify({ token: usernames, name: usernames }));
          this.router.navigate(['/coach']);
        }
        else if (res == '0') {
          alert('usernames and Password is incorrect please try again.')
        }
      })
      .catch();
  }
  
  forgotpassword() {
    this.router.navigateByUrl('/forgotpassword')
  }
register(){
  this.router.navigateByUrl('/register')
}
  ngOnInit() {
  }

}
