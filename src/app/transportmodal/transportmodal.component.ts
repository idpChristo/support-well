import { Component, OnInit, ViewContainerRef, ComponentRef } from '@angular/core';
import { ModalDialogService, IModalDialogButton, IModalDialog, IModalDialogOptions } from 'ngx-modal-dialog';

@Component({
  selector: 'app-transportmodal',
  templateUrl: './transportmodal.component.html',
  styleUrls: ['./transportmodal.component.scss']
})
export class TransportmodalComponent implements OnInit {

  actionButtons: IModalDialogButton[];


  constructor(modalService: ModalDialogService, viewRef: ViewContainerRef) {
    this.actionButtons = [
      { text: 'Close' }, // no special processing here
      { text: 'I will always close', onAction: () => true },
      { text: 'I never close', onAction: () => false }
    ];
   }

   dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
    // no processing needed
  }

  ngOnInit() {
  }

}
