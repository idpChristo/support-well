import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportmodalComponent } from './transportmodal.component';

describe('TransportmodalComponent', () => {
  let component: TransportmodalComponent;
  let fixture: ComponentFixture<TransportmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
