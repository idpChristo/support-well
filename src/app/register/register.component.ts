import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../models/UserProfileModel'
import { UserService } from "../services/user.service";
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  FirstName: any;
  LastName: any;
  EmailAddress: any;
  CellPhone: any;
  Password: any;
  ConfirmPassword: any;
  registerForm: FormGroup;


  error_messages = {
    'username': [
      { type: 'required', message: 'Your Cellphone number is required' },
      { type: 'minlength', message: 'Cellphone minimun length is 10 Numbers' },
      { type: 'maxlength', message: 'Cellphone maximum length is 13 Numbers' },
    ],
    'password': [
      { type: 'required', message: 'Your Password is required' },
      { type: 'minlength', message: 'Password minimun length is 4 Characters' },
      { type: 'maxlength', message: 'Password maximum length is 99 Characters' },
    ],
    'lastname': [
      { type: 'required', message: 'Your Last Name is required' },
      { type: 'minlength', message: 'Last Name minimun length is 4 Characters' },
      { type: 'maxlength', message: 'Last Name maximum length is 99 Characters' },
    ],
    'firstname': [
      { type: 'required', message: 'Your First Name is required' },
      { type: 'minlength', message: 'First Name minimun length is 4 Characters' },
      { type: 'maxlength', message: 'First Name maximum length is 99 Characters' },
    ],
    'email': [
      { type: 'minlength', message: 'Email Address minimun length is 4 Characters' },
      { type: 'maxlength', message: 'Email Address maximum length is 99 Characters' },
      { type: 'patter', message: 'Email Address does not meet validation requirements' },
    ]
  }

  constructor(public formBuilder: FormBuilder, public service: UserService, public router: Router) {
    this.user = new UserProfile(0, '', '', '', '', '', '');
    this.registerForm = this.formBuilder.group({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(99)
      ])),
      username: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(13),

      ])),
      firstname: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),

      ])),
      lastname: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),

      ])),
      email: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(150),
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      }
    );
  }
  register() {

    const firstname = this.registerForm.get('firstname').value;
    const lastname = this.registerForm.get('lastname').value;
    const password = this.registerForm.get('password').value;
    const cellphone = this.registerForm.get('username').value;
    const email = this.registerForm.get('email').value;
    this.service.register(firstname, lastname, email, cellphone, password).subscribe(res => {

      if (res === 'true') {
        alert('CellPhone or Email has been used before please correct this.');

      }
      else if (res === 'false') {
        alert('Thank you for signing up as a supporter - We look forward to interacting with you via the App, You will receive an SMS on approval of your registration');
        this.router.navigateByUrl('/login');
      }
    });
  }
  login() {
    this.router.navigateByUrl('/login');
  }
  ngOnInit() {
  }

}
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}