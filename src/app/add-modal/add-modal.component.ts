import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { ModalData } from "../models/ModalData";

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.scss']
})
export class AddModalComponent implements OnInit {

  BookingID: any;
  ServeType: any;
  Slots: any;
  Slot: any;
  StartTime: any;
  EndTime: any;
  categoryList: any;
  slots: any;
  Type: any;
  dataReturned: any;
  eventList: any;
  list: any;
  list1: any;
  list2: any;
  username: any;
  eventName: string;
  eventlist: string;
  typelist: any;
  fileUrl: any;
  FullName: any;
  EventsName: any;
  EventsType: any;
  show: boolean;




  constructor(public dialogRef: MatDialogRef<AddModalComponent>, @Inject(MAT_DIALOG_DATA) public data: ModalData, public http: HttpClient) {
    this.getCategories();
    this.getslots();

  }

  TypeChanged(selectedValue: string) {
    this.Type = selectedValue;
  }

  SlotChanged(selectedValue: string) {
    this.Slot = selectedValue;
  }
  
  add(StartTime) {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/AddSchedule?ServeType=' + this.Type + '&Slots=' + this.Slot + '&StartTime=' + StartTime).subscribe(res => {

    })
    this.onNoClick();
  }

  getCategories() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetCategories').subscribe(res => {
      this.categoryList = res;
    })
  }

  getslots() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/getSlots').subscribe(res => {
      this.slots = res;
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
