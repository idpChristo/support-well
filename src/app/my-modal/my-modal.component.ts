import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { ModalData } from "../models/ModalData";
@Component({
  selector: 'app-my-modal',
  templateUrl: './my-modal.component.html',
  styleUrls: ['./my-modal.component.scss']
})
export class MyModalComponent implements OnInit {
  BookingID: any;
  ServeType: any;
  Slots: any;
  Slot: any;
  StartTime: any;
  EndTime: any;
  categoryList: any;
  slots: any;
  Type: any;

  constructor(public dialogRef: MatDialogRef<MyModalComponent>, @Inject(MAT_DIALOG_DATA) public data: ModalData, public http: HttpClient) {
    this.BookingID = data.BookingID;
    this.ServeType = data.BookingName;
    this.Slots = data.Slots;
    this.getCategories();
    this.getslots();
  }

  TypeChanged(selectedValue: string) {
    this.Type = selectedValue;
  }

  SlotChanged(selectedValue: string) {
    this.Slot = selectedValue;
  }
  getCategories() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/GetCategories').subscribe(res => {
      this.categoryList = res;
    })
  }

  getslots() {
    this.http.get('https://supportwellapi.idp-apps.com:449/api/getSlots').subscribe(res => {
      this.slots = res;
    })
  }
  Update(StartTime) {
debugger;
    if (this.Slot == 'undefined' || this.Slot == '' || this.Slot == null) {
      this.Slot = this.Slots
    }
    if (this.Type == 'undefined' || this.Type == '' || this.Type == null) {
      this.Type = this.ServeType
    }
    this.http.get('https://supportwellapi.idp-apps.com:449/api/updateSchedule?ServeType=' + this.Type + '&Slots=' + this.Slot + '&BookingID=' + this.BookingID + '&StartEndTime=' + StartTime).subscribe(res => {

      this.onNoClick();
    })
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
