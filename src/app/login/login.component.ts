import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import * as $ from 'jquery';

// import { FormGroup, FormBuilder, FormControl, Validators, Form } from "@angular/forms";
// import { ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    //   styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: any;
    result: any;
    usernames: string;
    password: string;

    constructor(
        public router: Router,
        public http: HttpClient,
    ) { }

    ngAfterViewInit() {
        /*==================================================================
        [ Focus input ]*/
        $('.input100').each(function () {
            $(this).on('blur', function () {
                if ($(this).val() != "") {
                    $(this).addClass('has-val');
                }
                else {
                    $(this).removeClass('has-val');
                }
            })
        })
    }

    ngOnChanges() {
        (function ($) {
            "use strict";


            /*==================================================================
            [ Validate ]*/
            var input = $('.validate-input .input100');

            function showValidate(input) {
                var thisAlert = $(input).parent();

                $(thisAlert).addClass('alert-validate');
            }

            function validate(input) {
                // if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
                //     if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                //         return false;
                //     }
                // }
                // else {
                //     if($(input).val().trim() == ''){
                //         return false;
                //     }
                // }
            }

            $('.validate-form').on('submit', function () {
                var check = true;

                // for(var i=0; i<input.length; i++) {
                //     if(validate(input[i]) == false){
                //         showValidate(input[i]);
                //         check=false;
                //     }
                // }

                if (input.length === 0) {
                    check = false;
                    showValidate(input);
                } else if (typeof input !== "string") {
                    check = false;
                    showValidate(input);
                }

                return check;
            });


            $('.validate-form .input100').each(function () {
                $(this).focus(function () {
                    hideValidate(this);
                });
            });




            function hideValidate(input) {
                var thisAlert = $(input).parent();
                $(thisAlert).removeClass('alert-validate');
            }


        })(jQuery);
    }

    // login(usernames, password) {
    //     //const password = this.loginForm.get('password').value;
    //     //const usernames = this.loginForm.get('usernames').value;
    //     const test = this.loginForm.get('test').value;

    //     return this.http.get('https://supportwellapi.idp-apps.com:449/api/login?username=' + usernames + '&password=' + password)
    //         .toPromise()
    //         .then(res => {
    //             this.result = res;
    //             console.log('Status: ' + res)
    //             if (res == '1') {
    //                 localStorage.setItem('currentUser', JSON.stringify({ token: usernames, name: usernames }));
    //                 this.router.navigate(['/home']);
    //                 //this.router.navigate(['/coach']);
    //             }
    //             if (res == '4') {
    //                 localStorage.setItem('currentUser', JSON.stringify({ token: usernames, name: usernames }));
    //                 this.router.navigate(['/coach']);
    //             }
    //             else if (res == '0') {
    //                 alert('usernames and Password is incorrect please try again.')
    //             }
    //         })
    //         .catch();
    // }

    ngOnInit() { }
}

